terraform {
  backend "s3" {
    bucket         = "my-bucket-clearsense"
    key            = "terraform.tfstate"
    region         = "us-east-2"  # Set the AWS region for the S3 bucket
    encrypt        = true
  }
}